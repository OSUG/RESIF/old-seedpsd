# encoding: utf8
import os
import re
import glob
import time
import logging
import multiprocess
from obspy.clients.fdsn import Client
from .npz import NPZFileFactory, NPZFile


regex_filename_mseed = re.compile('^(?P<network>[A-Z0-9]+)\.(?P<station>[A-Z0-9]+)\.(?P<location>[A-Z0-9]*)\.(?P<channel>[A-Z0-9]+)\.(?P<quality>[A-Z]{1})\.(?P<year>[0-9]{4})\.(?P<day>[0-9]{3})$')
regex_filename_npz = re.compile('^(?P<network>[A-Z0-9]+)\.(?P<station>[A-Z0-9]+)\.(?P<location>[A-Z0-9]*)\.(?P<channel>[A-Z0-9]+)\.(?P<quality>[A-Z]{1})\.(?P<year>[0-9]{4})\.npz$')
regex_filename_dirty = re.compile('^(?P<network>[A-Z0-9]+)\.(?P<station>[A-Z0-9]+)\.(?P<location>[A-Z0-9]*)\.(?P<channel>[A-Z0-9]+)\.(?P<quality>[A-Z]{1})\.(?P<year>[0-9]{4})\.dirty$')


class SeedPsdWorker(object):
    """
    SeedPSD Worker class
    """

    def __init__(self, input_dir, output_dir, parallel=False, max_cpu=None):
        logging.debug("SeedPsdWorker.__init__(%s, %s, %s, %s)" % (input_dir, output_dir, parallel, max_cpu))

        # ObsPy client
        self.resif = Client("RESIF")

        # Parallelization
        if max_cpu:
            self.n_cores = min(len(os.sched_getaffinity(0)), int(max_cpu))
        else:
            self.n_cores = len(os.sched_getaffinity(0))

        if parallel and self.n_cores > 1:
            logging.info("Parallelization ENABLED. Set CPU limit to %i" % self.n_cores)
            self._parallel = parallel
        elif parallel and self.n_cores == 1:
            logging.warning("Only 1 CPU detected. Parallelization DISABLED")
            self._parallel = False
        else:
            logging.warning("Parallelization DISABLED")
            self._parallel = False

        # Check access to input/output dirs
        self._input_dir = input_dir
        self._output_dir = output_dir

        if not os.access(input_dir, os.R_OK):
            logging.error("The input directory '%s' is not readable" % input_dir)
            raise Exception("The input directory '%s' is not readable" % input_dir)

        if not os.access(output_dir, os.W_OK):
            logging.error("The output directory '%s' is not writeable" % output_dir)
            raise Exception("The output directory '%s' is not writeable" % output_dir)

        # Initialize the NPZ file factory
        self._NPZFactory = NPZFileFactory(input_dir=input_dir, output_dir=output_dir)

    # ENTRY POINT: AMQP MESSAGE ########################################################################################

    def process_amqp_message(self, amqp_message, separator='\n'):
        """
        Add an AMQP message containing a list of MSEED files to process

        :param amqp_message: The AMQP message
        :param separator: The AMQP message separator (default='\n')
        """
        logging.debug("SeedPsdWorker.process_amqp_message(%s, %s)" % (amqp_message, separator))

        npz_files = {}
        for mseed_file_path in amqp_message.split(separator):
            try:
                npz_file = self._NPZFactory.from_mseed_filename(mseed_file_path, raise_error=True)

                # Ignore temporary networks (SISMOB)
                if npz_file.is_temporary_network:
                    logging.info("Rejecting a part of the AMQP message (temporary network): '%s'" % mseed_file_path)
                else:
                    npz_filename = npz_file._file_name
                    logging.debug("Processing '%s'" % npz_filename)

                    if npz_filename not in npz_files:
                        npz_files[npz_filename] = npz_file

                    npz_files[npz_filename].add_mseed_file(mseed_file_path)
            except Exception:
                logging.warning("Rejecting a part of the AMQP message: '%s'" % mseed_file_path)

        # Process each file
        if self._parallel:
            with multiprocess.Pool(self.n_cores) as pool:
                pool.map(lambda file: file.update(), npz_files.values())
        else:
            for npz_file in npz_files.values():
                npz_file.update()

    # ENTRY POINT: PROCESS FILES #######################################################################################

    def process_file(self, file, action='update'):
        """
        Process a file name or path

        :param file: The file name/path to process
        :param action: The action (build, update, clean)
        """
        logging.debug("SeedPsdWorker.process_file(%s, %s)" % (file, action))
        if isinstance(file, (list, tuple)):
            for f in file:
                self.process_file(f, action)

        elif isinstance(file, NPZFile):
            file.add_mseed_files(self._mseed_path(file._year, file._network, file._station, file._channel))
            return self._process_file_npz(file, action)

        elif regex_filename_mseed.match(os.path.basename(file)):
            logging.info("Processing MSEED file: %s" % file)
            npz_file = self._NPZFactory.from_mseed_filename(file)
            npz_file.add_mseed_file(file)
            return self._process_file_npz(npz_file, action)

        elif regex_filename_npz.match(os.path.basename(file)):
            logging.info("Processing NPZ file: %s" % file)
            npz_file = self._NPZFactory.from_npz_filename(file)
            npz_file.add_mseed_files(self._mseed_path(npz_file._year, npz_file._network, npz_file._station, npz_file._channel))
            return self._process_file_npz(npz_file, action)

        elif regex_filename_dirty.match(os.path.basename(file)):
            logging.info("Processing DIRTY file: %s" % file)
            npz_file = self._NPZFactory.from_npz_filename(file.replace('.dirty', '.npz'))
            npz_file.add_mseed_files(self._mseed_path(npz_file._year, npz_file._network, npz_file._station, npz_file._channel))
            return self._process_file_npz(npz_file, action)

        else:
            logging.warning("Not supported file pattern for '%s'" % file)

    @staticmethod
    def _process_file_npz(npz_file, action='update'):
        """
        Process a NPZ file

        :param npz_file: The NPZ file
        :param action: The action (build, update, clean)
        """
        logging.debug("SeedPsdWorker._process_file_npz(%s, %s)" % (npz_file, action))

        # Save the NPZ file
        if action == 'build':
            tic = time.time()
            npz_file.build()
            toc = round(time.time() - tic, 2)
            logging.info("%s processed in %s seconds" % (npz_file._file_name, toc))

        elif action == 'update':
            tic = time.time()
            npz_file.update()
            toc = round(time.time() - tic, 2)
            logging.info("%s processed in %s seconds" % (npz_file._file_name, toc))

        elif action == 'clean':
            tic = time.time()
            ok = npz_file.build()
            if ok:
                npz_file.clean_dirty()
            toc = round(time.time() - tic, 2)
            logging.info("%s processed in %s seconds" % (npz_file._file_name, toc))

        else:
            logging.warning("Not supported action '%s' for file '%s'" % (action, npz_file))

    # ENTRY POINT: FIND DIRTY NPZ FILES ################################################################################

    def find_dirty(self, year='*', network='*'):
        """
        Find dirty NPZ files

        :param year: The year
        :param network: The network code
        """
        logging.debug("SeedPsdWorker.find_dirty(%s, %s)" % (year, network))
        dirty_files = glob.glob(os.path.join(self._output_dir, network, year, '*.dirty'))
        logging.info("%i dirty file(s) found!" % len(dirty_files))

        if self._parallel and len(dirty_files) > 1:
            with multiprocess.Pool(self.n_cores) as pool:
                pool.map(lambda file: self.process_file(file, action='clean'), dirty_files)
        else:
            for file in dirty_files:
                self.process_file(file, action='clean')

    # ENTRY POINT: FIND MSEED FILES ####################################################################################

    def find_mseed(self, year, network, station=None, channel=None, location=None, update=False):
        """
        Find MSEED files and add them to related NPZ files

        :param year: The year (or a list of years separated by comma)
        :param network: The network code (or a list of codes separated by comma)
        :param station: The station code
        :param channel: The channel code
        :param location: The location code
        :param update: Update the NPZ file (if exists)
        """
        logging.debug("SeedPsdWorker.find_mseed(%s, %s, %s, %s, %s, %s)" % (year, network, station, channel, location, update))

        # Initialize the list of NPZ to process
        npz_files = {}

        # Get an inventory
        inventory = self._get_inventory(network, station, location, channel)
        if inventory:

            # Process each year
            for year in str(year).split(','):

                # Process each network
                for network in inventory:

                    # Extract network code
                    network_code = network.code

                    # Check network/year directories
                    if os.path.exists(self._mseed_path(year, network_code)):

                        # Process each station
                        for station in network.stations:

                            # Extract station code
                            station_code = station.code

                            # Check station directories
                            if os.path.exists(self._mseed_path(year, network_code, station_code)):

                                # Build the condensed list of channels
                                channels = {}
                                for channel in station.channels:
                                    if channel.code not in channels:
                                        channels[str(channel.code)] = []
                                    if channel.location_code not in (None, '') and channel.location_code not in channels[str(channel.code)]:
                                        channels[str(channel.code)].append(str(channel.location_code))

                                # Process each channel
                                for channel_code, locations in channels.items():

                                    # Check channel directories
                                    if os.path.exists(self._mseed_path(year, network_code, station_code, channel_code)):
                                        for location_code in locations:
                                            npz_file = self._NPZFactory.factory(year, network_code, station_code, location_code, channel_code)
                                            npz_files[npz_file._file_name] = npz_file

                                    else:
                                        logging.warning("No directory found for channel '%s' of station '%s.%s' in '%s" % (channel_code, network_code, station_code, year))
                            else:
                                logging.warning("No directory found for station '%s.%s' in '%s" % (network_code, station_code, year))
                    else:
                        logging.warning("No directory found for network '%s' in '%s" % (network_code, year))
        else:
            logging.debug("No inventory found")

        # Determinate the action to do
        if update:
            action = 'update'
        else:
            action = 'build'

        # Process each NPZ file found
        logging.info("Found %i NPZ file(s) to %s" % (len(npz_files), action))
        if self._parallel and len(npz_files) > 1:
            with multiprocess.Pool(self.n_cores) as pool:
                pool.map(lambda file: self.process_file(file, action=action), npz_files.values())
        else:
            for file in npz_files.values():
                self.process_file(file, action=action)

    # TOOLS ############################################################################################################

    def _get_inventory(self, network, station, location=None, channel=None):
        """
        Get an inventory filtered by network, station, channel

        :param network: The network code
        :param station: The station code
        :param channel: The location code
        :param channel: The channel code
        :return: An inventory
        """
        logging.debug("SeedPsdWorker._get_inventory(%s, %s, %s, %s)" % (network, station, location, channel))

        if channel is not None:
            channel = channel.split(".")[0]

        return self.resif.get_stations(network=network, station=station, location=location, channel=channel, level="channel")

    def _mseed_path(self, year, network, station=None, channel_code=None):
        """
        Build a full path for a validated data directory

        :param year: The year
        :param network: The network code
        :param station: The station code
        :param channel_code: The channel code
        :return: The data file path
        :rtype: str
        """
        logging.debug("SeedPsdWorker._mseed_path(%s, %s, %s, %s)" % (year, network, station, channel_code))

        path = os.path.join(self._input_dir, network, str(year))
        if station:
            path = os.path.join(path, station)
            if channel_code:
                path = os.path.join(path, '%s.D' % channel_code)

        logging.debug("> Generated path: %s" % path)
        return path
