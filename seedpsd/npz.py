# encoding: utf8
import os
import re
import glob
import logging
from obspy import read
from obspy.signal import PPSD
from obspy.clients.fdsn import Client


regex_mseed_filename = re.compile('^(?P<network>[A-Z0-9]+)\.(?P<station>[A-Z0-9]+)\.(?P<location>[A-Z0-9]*)\.(?P<channel>[A-Z0-9]+)\.(?P<quality>[A-Z]{1})\.(?P<year>[0-9]{4})\.(?P<day>[0-9]{3})$')
regex_npz_filename = re.compile('^(?P<network>[A-Z0-9]+)\.(?P<station>[A-Z0-9]+)\.(?P<location>[A-Z0-9]*)\.(?P<channel>[A-Z0-9]+)\.(?P<quality>[A-Z]{1})\.(?P<year>[0-9]{4})\.npz$')
regex_temp_network = re.compile("^[XYZ0-9][A-Z0-9]?$", flags=re.IGNORECASE)


# NPZ FILE  FACTORY ####################################################################################################
########################################################################################################################

class NPZFileFactory(object):
    """
    NPZFile factory class
    """

    def __init__(self, input_dir, output_dir):
        """
        Initialize the NPZFile factory

        :param input_dir: The directory path containing the MSEED files
        :param output_dir: The directory path containing the NPZ files
        """
        logging.debug("NPZFileFactory.__init__(%s, %s)" % (input_dir, output_dir))
        self._mseed_path = input_dir
        self._npz_path = output_dir

    def factory(self, year, network, station, location, channel, quality='D', inventory=None):
        """
        Initialize a NPZFile object

        :param year: The year
        :param network: The network code
        :param station: The station code
        :param location: The location code
        :param channel: The channel code
        :param quality:
        :param inventory: An ObsPy inventory (optional)
        :rtype: NPZFile
        """
        logging.debug("NPZFileFactory.factory(%s, %s, %s, %s, %s, %s)" % (year, network, station, location, channel, quality))
        return NPZFile(year=year,
                       network=network,
                       station=station,
                       location=location,
                       channel=channel,
                       quality=quality,
                       npz_path=self._npz_path,
                       mseed_path=self._mseed_path,
                       inventory=inventory)

    def from_mseed_filename(self, file_path, raise_error=False):
        """
        Initialize a NPZFile object from a MSEED filename

        :param file_path: The MSEED file path
        :param raise_error: Raise an Exception on error
        :rtype: NPZFile
        """
        logging.debug("NPZFileFactory.from_mseed_filename(%s, %s)" % (file_path, raise_error))

        # Extract the filename from the path
        filename = os.path.basename(file_path)

        # Check the filename against a regex pattern
        match = regex_mseed_filename.match(filename)
        if match:

            # Extract parts from the filename
            mdict = match.groupdict()

            return self.factory(year=mdict['year'],
                                network=mdict['network'],
                                station=mdict['station'],
                                location=mdict['location'],
                                channel=mdict['channel'],
                                quality=mdict['quality'])
        elif raise_error:
            raise Exception("Unable to match a filename pattern for '%s'" % file_path)
        else:
            logging.warning("Unrecognized filename pattern for '%s'" % file_path)

    def from_npz_filename(self, file_path, raise_error=False):
        """
        Initialize a NPZFile object from a NPZ filename

        :param file_path: The NPZ file path
        :param raise_error: Raise an Exception on error
        :rtype: NPZFile
        """
        logging.debug("NPZFileFactory.from_npz_filename(%s, %s)" % (file_path, raise_error))

        # Extract the filename from the path
        filename = os.path.basename(file_path)

        # Check the filename against a regex pattern
        match = regex_npz_filename.match(filename)
        if match:

            # Extract parts from the filename
            mdict = match.groupdict()

            return self.factory(year=mdict['year'],
                                network=mdict['network'],
                                station=mdict['station'],
                                location=mdict['location'],
                                channel=mdict['channel'],
                                quality=mdict['quality'])
        elif raise_error:
            raise Exception("Unable to match a filename pattern for '%s'" % file_path)
        else:
            logging.warning("Unrecognized filename pattern for '%s'" % file_path)


# NPZ FILE #############################################################################################################
########################################################################################################################

class NPZFile(object):
    """
    NPZFile class
    """

    def __init__(self, year, network, station, location, channel, quality, npz_path, mseed_path, inventory=None):
        logging.debug("NPZFile.__init__(%s, %s, %s, %s, %s, %s)" % (year, network, station, location, channel, quality))
        self._year = year
        self._network = network
        self._station = station
        self._location = location
        self._channel = channel
        self._quality = quality
        self._npz_base_path = npz_path
        self._mseed_base_path = mseed_path
        self._inventory = inventory
        self._mseed_files = {}

    def __repr__(self):
        return '<NPZFile %s' % self._file_name

    # PROPERTIES #######################################################################################################

    @property
    def _file_name(self):
        """
        Generate the NPZ file name

        :rtype: str
        """
        #logging.debug("NPZFile._file_name()")
        return '%s.%s.%s.%s.%s.%s.npz' % (self._network, self._station, self._location, self._channel, self._quality, self._year)

    @property
    def _file_path(self):
        """
        Generate the NPZ file path

        :rtype: str
        """
        #logging.debug("NPZFile._file_path()")
        return os.path.join(self._npz_base_path, self._network, self._year, self._file_name)

    @property
    def _metadata(self):
        """
        Get the metadata related to the NPZ file

        :return: An ObsPy inventory object
        :rtype: obspy.core.inventory.inventory.Inventory
        """
        #logging.debug("NPZFile._metadata()")
        if self._inventory is None:
            self._inventory = Client("RESIF").get_stations(network=self._network, station=self._station, location=self._location, channel=self._channel, level="response")
            logging.debug("%s: Inventory: %s" % (self._file_name, self._inventory))
        return self._inventory

    @property
    def exists(self):
        """
        Check if the NPZ file exist

        :rtype: bool
        """
        #logging.debug("NPZFile.exists()")
        return os.path.exists(self._file_path)

    @property
    def is_dirty(self):
        """
        Check if the NPZ file is dirty

        :rtype: bool
        """
        #logging.debug("NPZFile.is_dirty()")
        return os.path.exists(str(self._file_path).replace('.npz', '.dirty'))

    @property
    def is_temporary_network(self):
        """
        Return True if the network is temporary

        :rtype: bool
        """
        if regex_temp_network.match(self._network):
            return True
        else:
            return False

    # MSEED FILES ######################################################################################################

    def add_mseed_files(self, path):
        """
        Add all MSEED files from a specified path

        :param path: The directory path
        """
        logging.debug("%s: NPZFile.add_mseed_files(%s)" % (self._file_name, path))
        pattern = '%s.%s.%s.%s.D.%s.*' % (self._network, self._station, self._location, self._channel, self._year)
        for file in glob.glob(os.path.join(path, pattern)):
            self.add_mseed_file(file)

    def add_mseed_file(self, mseed_file_path):
        """
        Add a MSEED file to process

        :param mseed_file_path: The MSEED file path
        :rtype: bool
        """
        logging.debug("%s: NPZFile.add_mseed_file(%s)" % (self._file_name, mseed_file_path))

        # Extract the filename from the path
        filename = os.path.basename(mseed_file_path)

        # Check the filename against a regex pattern
        match = regex_mseed_filename.match(filename)
        if match:

            # Extract parts from the filename
            mdict = match.groupdict()

            # Check if the MSEED file match the NPZ file
            if mdict['year'] == self._year and mdict['network'] == self._network and mdict['station'] == self._station \
                    and mdict['location'] == self._location and mdict['channel'] == self._channel \
                    and mdict['quality'] == self._quality:

                # Build the full file path
                file_path = os.path.join(self._mseed_base_path, self._network, self._year, self._station, '%s.D' % self._channel, filename)

                # Check if the file exists
                if os.path.exists(file_path):

                    # Add the file to the process list
                    self._mseed_files[mdict['day']] = file_path
                    return True
                else:
                    logging.warning("%s: Ignoring the missing MSEED file '%s'" % (self._file_name, file_path))
                    return False
            else:
                logging.warning("%s: The MSEED file '%s' is not compatible with the NPZ file" % (self._file_name, mseed_file_path))
                return False
        else:
            logging.warning("%s: Ignoring file '%s'" % (self._file_name, mseed_file_path))
            return False

    # SAVE #############################################################################################################

    def build(self, ppsd=None):
        """
        Build a NPZ file from MSEED data

        :param ppsd: The PPSD object (to update)
        :rtype: bool
        """
        logging.debug("%s: NPZFile.save(%s)" % (self._file_name, ppsd))
        try:
            # Parse MSEED files
            for day in sorted(self._mseed_files.keys()):
                file_path = self._mseed_files[day]
                logging.info("%s: Import day %s from '%s' (MSEED)" % (self._file_name, day, file_path))
                stream = read(file_path, format="MSEED")
                ppsd = ppsd or PPSD(stream[0].stats, metadata=self._metadata)
                ppsd.add(stream)

            # Write NPZ file
            return self._write_file(ppsd)

        except Exception as ex:
            logging.error(str(ex))
            logging.error("%s: Error during build of NPZ file", self._file_name)
            return False

    def update(self):
        """
        Update the NPZ file with missing MSEED data

        :rtype: bool
        """
        logging.debug("%s: NPZFile.update()" % self._file_name)

        if not self.exists:
            logging.warning("%s: NZP file does NOT exists. Creating %s" % (self._file_name, self._file_path))
            return self.build()

        elif self.is_dirty:
            logging.warning("%s: NZP file is dirty. Skipping..." % self._file_name)
            return False

        else:
            try:
                logging.info("%s: Loading existing NZP file '%s' for update..." % (self._file_name, self._file_path))
                ppsd = PPSD.load_npz(self._file_path, metadata=self._metadata)

                # Get the days already processed by the NPZ
                npz_days = set([td[0].julday for td in ppsd.times_data])
                logging.debug('%s: Already processed days found in NPZ: %s' % (self._file_name, npz_days))

                # Processing only missing files
                npz_updated = False
                for day in sorted(self._mseed_files.keys()):
                    if int(day) not in npz_days:
                        file_path = self._mseed_files[day]
                        logging.info("%s: Import day %s from '%s' (MSEED)" % (self._file_name, day, file_path))
                        stream = read(file_path, format="MSEED")
                        ppsd.add(stream)

                        npz_updated = True
                    else:
                        logging.debug("%s: The day %s has already been processed by the NPZ. Ignoring %s" % (self._file_name, day, self._mseed_files[day]))

                # Write the NPZ file
                if npz_updated:
                    return self._write_file(ppsd)
                else:
                    logging.info("%s: NPZ file NOT changed", self._file_name)
                    return True

            except Exception as ex:
                logging.error(str(ex))
                logging.error("%s: Error during update of NPZ file", self._file_name)
                return False

    def _write_file(self, ppsd):
        """
        Write the NPZ file on disk

        :param ppsd: The PPSD object
        :rtype: bool
        """
        logging.debug("%s: NPZFile._write_file(%s)" % (self._file_name, ppsd))
        if ppsd is not None:
            try:
                # Check existence of output directory
                os.makedirs(os.path.join(self._npz_base_path, self._network, self._year), exist_ok=True)

                # Save NPZ file
                logging.info("%s: Saving NPZ to '%s'" % (self._file_name, self._file_path))
                ppsd.save_npz(self._file_path)
                return True

            except Exception as ex:
                logging.error(str(ex))
                logging.error("%s: An error occurred during save of NPZ file", self._file_name)
                return False
        else:
            return False

    def clean_dirty(self):
        """
        Cleaning the 'dirty' flag for this NPZ file
        """
        if self.is_dirty:
            logging.info("%s: Cleaning dirty flag" % self._file_name)
            os.remove(str(self._file_path).replace('.npz', '.dirty'))
        else:
            logging.debug("%s: The NPZ file is NOT dirty, nothing to do!" % self._file_name)

    ####################################################################################################################
    # def infos(self):
    #     ppsd = PPSD.load_npz(self._file_path, metadata=self._metadata)
    #
    #     print("> Times processed")
    #     for tp in ppsd.times_processed:
    #         print(">> %s" % tp)
    #
    #     print("> Times data")
    #     for td in ppsd.times_data:
    #         print(">> %s | %s" % (td[0], td[1]))
    #
    #     print("> Times gap")
    #     for tg in ppsd.times_gaps:
    #         print(">> %s | %s" % (tg[0], tg[1]))
    #
    # def plot(self):
    #     ppsd = PPSD.load_npz(self._file_path, metadata=self._metadata)
    #     ppsd.plot(str(self._file_path).replace('.npz', '.png'))
