# encoding: utf8
import logging
from proton.handlers import MessagingHandler
from proton.reactor import Container, Copy

logger = logging.getLogger(__name__)


# AMQP MESSAGE HANDLER
########################################################################################################################

class SeedPsdAMQPMessageHandler(MessagingHandler):
    """
    AMQP message handler for SeedPSD
    """

    def __init__(self, server, queue, worker, dry_run=False):
        """
        Initialize an AMQP Message handler for SeedPSD
        :param server: The AMQP broker server URL
        :param queue: The AMQP queue
        :param worker: The SeedPsdWorker instance
        :param dry_run: Allows to not consume the message from queue and not register anything to database
        """
        logging.debug("SeedPsdAMQPMessageHandler.__init__(%s, %s, %s, %s)" % (server, queue, worker, dry_run))
        super(SeedPsdAMQPMessageHandler, self).__init__()
        self._server = server
        self._queue = queue
        self._worker = worker
        self._dry_run = dry_run

    def on_start(self, event):
        logging.debug("SeedPsdAMQPMessageHandler.on_start(%s)" % event)

        logger.info("Connecting to %s:%s", self._server, self._queue)
        connection = event.container.connect(self._server)
        if self._dry_run:
            event.container.create_receiver(connection, self._queue, name='seedpsd', options=Copy())
            logger.info("Connected in browse mode (will not consume the messages))")
        else:
            event.container.create_receiver(connection, self._queue, name='seedpsd')
            logger.info("Connected")

    def on_message(self, event):
        logging.debug("SeedPsdAMQPMessageHandler.on_message(%s)" % event)

        # Decode message
        if isinstance(event.message.body, bytes):
            message = str(event.message.body, 'utf-8')
        else:
            message = event.message.body

        logger.debug("Received message: %s", message)

        # Send message to worker
        self._worker.process_amqp_message(message)

# AMQP CLIENT
########################################################################################################################

class SeedPsdAMQPClient(object):
    """
    AMQP client for SeedPSD
    """

    def __init__(self, broker, queue):
        """
        Initialize an AMQP Client for SeedPSD

        :param broker: The AMQP broker serveur URL
        :param queue: The AMQP queue
        """
        logging.debug("SeedPsdAMQPClient.__init__(%s, %s)" % (broker, queue))
        self.broker = broker
        self.queue = queue

    def connect(self, worker, dry_run=False):
        """
        Connect the AMQP client to the SeedPSD worker

        :param worker: The SeedPSD worker
        :param dry_run: Enable the 'dry_run' mode
        """
        logging.debug("SeedPsdAMQPClient.connect(%s, %s)" % (worker, dry_run))
        return Container(SeedPsdAMQPMessageHandler(self.broker, self.queue, worker, dry_run)).run()
