#!/usr/bin/env python
# encoding: utf8
import os
import sys
import click
import logging
import coloredlogs
from click_loglevel import LogLevel
from dotenv import load_dotenv
from .amqp import SeedPsdAMQPClient
from .worker import SeedPsdWorker

# CONFIGURATION
########################################################################################################################
# Configure logger
log_format = '%(asctime)s %(levelname)s %(message)s'

# Load environment from ".env" file
load_dotenv()


# Group all sub-commands declared below
@click.group()
def cli():
    pass


# AMQP CLIENT
########################################################################################################################

@cli.command(help='Listen an AMQP queue')
@click.option('--broker', envvar='SEEDPSD_AMQP_BROKER', help='AMQP broker address:port')
@click.option('--queue', envvar='SEEDPSD_AMQP_QUEUE', help='AMQP queue name')
@click.option('--dry-run', 'dry_run', is_flag=True, envvar='SEEDPSD_AMQP_DRYRUN')
@click.option('--input_dir', type=click.Path(exists=True), envvar='SEEDPSD_INPUT_PATH', help='Path to directory containing validated data')
@click.option('--output_dir', type=click.Path(exists=True), envvar='SEEDPSD_OUTPUT_PATH', help='Path to directory containing NPZ data')
@click.option('--parallel/--serial', default=True, help='Enable parallelization')
@click.option('--max-cpu', 'max_cpu')
@click.option("--level", 'log_level', type=LogLevel(), default=logging.INFO)
def listen_amqp(broker, queue, dry_run, input_dir, output_dir, parallel, max_cpu, log_level):
    coloredlogs.install(level=log_level, fmt=log_format, stream=sys.stdout)
    try:
        logging.info("Starting AMQP client...")
        client = SeedPsdAMQPClient(broker, queue)

        logging.info("Starting a worker...")
        worker = SeedPsdWorker(input_dir=input_dir, output_dir=output_dir, parallel=parallel, max_cpu=max_cpu)

        logging.info("Connecting the worker to the AMQP queue '%s'..." % queue)
        client.connect(worker, dry_run)
        logging.info("Done.")

        sys.exit(os.EX_OK)
    except Exception as e:
        logging.debug(e)
        sys.exit(os.EX_SOFTWARE)

# WORKER
########################################################################################################################


@cli.command(help='Process NPZ files for a given NSLC')
@click.argument('year')
@click.argument('network')
@click.option('--station', help="set the station directory (default all)")
@click.option('--channel', help="set the channel directory (default all)")
@click.option('--location', help="set the location (default all)")
@click.option('--input_dir', type=click.Path(exists=True), envvar='SEEDPSD_INPUT_PATH', help='Path to directory containing validated data')
@click.option('--output_dir', type=click.Path(exists=True), envvar='SEEDPSD_OUTPUT_PATH', help='Path to directory containing NPZ data')
@click.option('--update/--build', 'update_mode', default=True, help='Build or Update mode')
@click.option('--parallel/--serial', default=True, help='Enable parallelization')
@click.option('--max-cpu', 'max_cpu')
@click.option("--level", 'log_level', type=LogLevel(), default=logging.INFO)
def process(year, network, station, channel, location, input_dir, output_dir, update_mode, parallel, max_cpu, log_level):
    coloredlogs.install(level=log_level, fmt=log_format, stream=sys.stdout)
    try:
        logging.info("Starting a worker...")
        worker = SeedPsdWorker(input_dir=input_dir, output_dir=output_dir, parallel=parallel, max_cpu=max_cpu)

        if update_mode:
            logging.info("Updating...")
        else:
            logging.info("Building...")

        worker.find_mseed(year=year,
                   network=network,
                   station=station,
                   location=location,
                   channel=channel,
                   update=update_mode)
        logging.info("Done.")

        sys.exit(os.EX_SOFTWARE)
    except Exception as e:
        logging.debug(e)
        sys.exit(os.EX_SOFTWARE)


@cli.command(help='Process a MSEED or NPZ file')
@click.argument('path')
@click.option('--action', default='update', type=click.Choice(['update', 'build', 'clean']), help="(default=update)")
@click.option('--input_dir', type=click.Path(exists=True), envvar='SEEDPSD_INPUT_PATH', help='Path to directory containing validated data')
@click.option('--output_dir', type=click.Path(exists=True), envvar='SEEDPSD_OUTPUT_PATH', help='Path to directory containing NPZ data')
@click.option("--level", 'log_level', type=LogLevel(), default=logging.INFO)
def process_file(path, action, input_dir, output_dir, log_level):
    coloredlogs.install(level=log_level, fmt=log_format, stream=sys.stdout)
    try:
        logging.info("Starting a worker...")
        worker = SeedPsdWorker(input_dir=input_dir, output_dir=output_dir)

        logging.info("Processing file '%s'..." % path)
        worker.process_file(path, action)
        logging.info("Done.")

        sys.exit(os.EX_SOFTWARE)
    except Exception as e:
        logging.debug(e)
        sys.exit(os.EX_SOFTWARE)


@cli.command(help="Find all 'dirty' files and rebuild the related NPZ files")
@click.option('--year', default='*', help="Year (YYYY)")
@click.option('--network',  default='*', help="Network code")
@click.option('--input_dir', type=click.Path(exists=True), envvar='SEEDPSD_INPUT_PATH', help='Path to directory containing validated data')
@click.option('--output_dir', type=click.Path(exists=True), envvar='SEEDPSD_OUTPUT_PATH', help='Path to directory containing NPZ data')
@click.option('--parallel/--serial', default=True, help='Enable parallelization')
@click.option('--max-cpu', 'max_cpu')
@click.option("--level", 'log_level', type=LogLevel(), default=logging.INFO)
def clean_dirty(year, network, input_dir, output_dir, parallel, max_cpu, log_level):
    coloredlogs.install(level=log_level, fmt=log_format, stream=sys.stdout)
    try:
        logging.info("Starting a worker...")
        worker = SeedPsdWorker(input_dir=input_dir, output_dir=output_dir, parallel=parallel, max_cpu=max_cpu)

        logging.info("Rebuilding dirty files...")
        worker.find_dirty(year=year, network=network)
        logging.info("Done.")

        sys.exit(os.EX_SOFTWARE)
    except Exception as e:
        logging.debug(e)
        sys.exit(os.EX_SOFTWARE)


# MAIN
########################################################################################################################

if __name__ == '__main__':
    cli()
