Changelog
=========

v2.0.0 (202Y-MM-DD)
-------------------

Refactoring:

* Move from ArgParse to Click
* ...

New features:

* Add AMQP listener
* Find and clean dirty files


v1.0.0 (202Y-MM-DD)
-------------------

* First release by Jérôme TOUVIER

v0.0.x (Unreleased)
-------------------

* Internal developments