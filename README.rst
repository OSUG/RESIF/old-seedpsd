SEEDPSD
=======

This software process miniseed data stream of the RESIF-DC. Hourly segmented power spectral densities (PSDs) are 
aggregated to show probabilistic distributions of the seismic noise levels. PSDs are calculated by the method described 
by McNamara, 2004 using ObsPy for a particular network/station/location/channel combination. Calculations are stored in 
npz archive format for the selected year and channel.

Installation
------------

From sources
^^^^^^^^^^^^

**seedpsd** sources are distributed under the terms of `GPLv3 licence <https://choosealicense.com/licenses/gpl-3.0>`_
and are available at https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/seedpsd

First, ensure Git and Python 3 are installed on your system.

.. code-block:: bash

    $ sudo apt install git
    $ sudo apt install python3 python3-virtualenv python3-pip python3-setuptools python3-wheel

Then, clone the project from the Gitlab repository:

.. code-block:: bash

    $ git clone https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/seedpsd.git
    $ cd seedpsd

Then, create and activate a virtualenv:

.. code-block:: bash

    $ python3 -m venv venv
    $ source venv/bin/activate

Finally, install the project and its dependencies:

.. code-block:: bash

    $ pip install --upgrade pip setuptools wheel
    $ pip install -e .

Configuration
-------------

Create a file '.env' to set the following environment variables:

.. code-block::

    SEEDPSD_AMQP_BROKER=amqp-geodata.ujf-grenoble.fr:5672
    SEEDPSD_AMQP_QUEUE=seedpsd
    SEEDPSD_AMQP_DRYRUN=False
    SEEDPSD_INPUT_PATH=/mnt/auto/archive/data/bynet
    SEEDPSD_OUTPUT_PATH=/mnt/nfs/summer/validated_seismic_metadata/ppsd

.. note::

    You will find skeletons inside the **.env-production** and **.env-test** files. Fill and uncomment the necessary lines.

Usage
-----

General syntax:

.. code-block:: bash

    $ seedpsd-cli [OPTIONS] COMMAND [ARGS]

Use the **--help** option to show the specific syntax of each command.

Available commands
------------------
.. code-block:: bash

    $ seedpsd-cli --help
    Usage: resif-datacite-cli [OPTIONS] COMMAND [ARGS]...

    Options:
      --help  Show this message and exit.

    Commands:
      clean-dirty   Find all 'dirty' files and rebuit the related NPZ files
      listen-amqp   Listen an AMQP queue
      process       Process NPZ files for a given NSLC
      process-file  Process a MSEED or NPZ file

Process
^^^^^^^

.. code-block:: bash

    $ seedpsd-cli process --help
    Usage: seedpsd-cli process [OPTIONS] YEAR NETWORK

      Process NPZ files for a given NSLC

    Options:
      --station TEXT       set the station directory (default all)
      --channel TEXT       set the channel directory (default all)
      --location TEXT      set the location (default all)
      --input_dir PATH     Path to directory containing validated data
      --output_dir PATH    Path to directory containing NPZ data
      --update / --build   Build or Update mode
      --level LOG_LEVEL    Either NOTSET, DEBUG, INFO, WARNING, ERROR or CRITICAL
      --help               Show this message and exit.

Process file
^^^^^^^^^^^^

.. code-block:: bash

    $ seedpsd-cli process-file --help
    Usage: seedpsd-cli process-file [OPTIONS] PATH

      Process a MSEED or NPZ file

    Options:
      --action [update|build|clean]  (default=update)
      --input_dir PATH               Path to directory containing validated data
      --output_dir PATH              Path to directory containing NPZ data
      --level LOG_LEVEL              Either NOTSET, DEBUG, INFO, WARNING, ERROR or CRITICAL
      --help                         Show this message and exit.

Clean dirty
^^^^^^^^^^^

.. code-block:: bash

    $ seedpsd-cli clean-dirty --help
    Usage: seedpsd-cli clean-dirty [OPTIONS]

      Find all 'dirty' files and rebuild the related NPZ files

    Options:
      --year TEXT          Year (YYYY)
      --network TEXT       Network code
      --input_dir PATH     Path to directory containing validated data
      --output_dir PATH    Path to directory containing NPZ data
      --level LOG_LEVEL    Either NOTSET, DEBUG, INFO, WARNING, ERROR or CRITICAL
      --help               Show this message and exit.

Listen AMQP
^^^^^^^^^^^

.. code-block:: bash

    $ seedpsd-cli listen-amqp --help
    Usage: seedpsd-cli listen-amqp [OPTIONS]

      Listen an AMQP queue

    Options:
      --broker TEXT        AMQP broker address:port
      --queue TEXT         AMQP queue name
      --dry-run
      --input_dir PATH     Path to directory containing validated data
      --output_dir PATH    Path to directory containing NPZ data
      --level LOG_LEVEL    Either NOTSET, DEBUG, INFO, WARNING, ERROR or CRITICAL
      --help               Show this message and exit.

Documentation
-------------
ObsPy
^^^^^

* https://docs.obspy.org/
* https://docs.obspy.org/tutorial/code_snippets/probabilistic_power_spectral_density.html
* https://github.com/obspy/obspy/wiki/Notes-on-Parallel-Processing-with-Python-and-ObsPy

Multiprocessing
^^^^^^^^^^^^^^^

* https://pypi.org/project/multiprocess/
* https://github.com/uqfoundation/multiprocess

Environment
^^^^^^^^^^^

* https://saurabh-kumar.com/python-dotenv/
* https://pypi.org/project/python-dotenv/

AMQP
^^^^

* https://qpid.apache.org/proton/index.html
* https://pypi.org/project/python-qpid-proton/

Click
^^^^^

* https://click.palletsprojects.com/
* https://coloredlogs.readthedocs.io/

* https://pypi.org/project/click/
* https://pypi.org/project/click-loglevel/
* https://pypi.org/project/coloredlogs/


Authors
-------

* Philippe Bollard
* Jérôme Touvier
